//
//  SurveyResultsViewController.m
//  NewCrmc
//
//  Created by MyMac on 8/6/14.
//  Copyright (c) 2014 webit. All rights reserved.
//
@interface SurveyResultsViewController ()
{
    NSArray*listSurvey;
    Survey*survey;
}
@end

@implementation SurveyResultsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    _tblResult=[[UITableView alloc]initWithFrame:CGRectMake(0, 216, 320, self.view.frame.size.height-238)];
    _tblResult.dataSource=self;
    _tblResult.delegate=self;
     _tblResult.separatorColor = [UIColor clearColor];
    _line1=[[UILabel alloc]initWithFrame:CGRectMake(0, 155, 320, 6)];
    _line1.backgroundColor=_color;
    [self.view addSubview:_line1];
    _line2=[[UILabel alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height-20, 320, 6)];
    _line2.backgroundColor=_color;
    [self.view addSubview:_line2];
    _lblSurveyDescription=[[UILabel alloc]initWithFrame:CGRectMake(2, 163, 300, 20)];
    _lblSurveyDescription.text=@"שאלות קודמות";
    _lblSurveyDescription.textAlignment=NSTextAlignmentRight;
    _lblSurveyDescription.font= [UIFont boldSystemFontOfSize:12.0f];
    [self.view addSubview:_lblSurveyDescription];
    _lbl2=[[UILabel alloc]initWithFrame:CGRectMake(2, 188, 300, 20)];
    _lbl2.text=@"בכל פעם ניתן לענות על שאלה אחת בלבד.";
    _lbl2.textAlignment=NSTextAlignmentRight;
    _lbl2.font= [UIFont boldSystemFontOfSize:12.0f];
    [self.view addSubview:_lbl2];
    [self.view addSubview:_tblResult];
    self.lblBackgroundTitle.backgroundColor=_color;
    self.Title.text=self.header;
    [self.generic showNativeActivityIndicator:self];
    [self.connection connectionToService:@"GetSurveyToCity" jsonDictionary:[self.appDelegate.city parseCityToDict] controller:self withSelector:@selector(GetSurveyToCityResult:)];
}

-(void)GetSurveyToCityResult:(NSString*)result
{
    [self.generic hideNativeActivityIndicator:self];
    if(![result isEqualToString:@""]&&![result isEqualToString:@"[]"])
    {
        survey=[[Survey alloc]init];
        listSurvey=[survey parseListSurveyFromJson:result];
        [_tblResult reloadData];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return listSurvey.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 81;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    SurveyResultTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell = [[SurveyResultTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.color=_color;
    [cell SetCell:listSurvey[indexPath.row]];
    return cell;
}
@end
