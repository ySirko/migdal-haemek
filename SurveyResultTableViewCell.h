//
//  SurveyResultTableViewCell.h
//  NewCrmc
//
//  Created by WebitMacMini Two on 8/6/14.
//  Copyright (c) 2014 webit. All rights reserved.
//
#import "Survey.h"
@interface SurveyResultTableViewCell : UITableViewCell
@property (retain, nonatomic) UILabel *lbl1;
@property (retain, nonatomic) UILabel *lbl2;
@property (retain, nonatomic)  UILabel *lblDiagram4;
@property (retain, nonatomic)  UILabel *lblDiagram3;
@property (retain, nonatomic)  UILabel *lblDiagram2;
@property (retain, nonatomic)  UILabel *lblDiagram1;
@property (retain, nonatomic)  UILabel *lblTitle;
@property (retain, nonatomic)  UILabel *lblNum4;
@property (retain, nonatomic)  UILabel *lblNum3;
@property (retain, nonatomic)  UILabel *lblNum2;
@property (retain, nonatomic)  UILabel *lblNum1;
@property (retain, nonatomic)  UILabel *lblOption1;
@property (retain, nonatomic)  UILabel *lblOption2;
@property (retain, nonatomic)  UILabel *lblOption3;
@property (retain, nonatomic)  UILabel *lblOption4;
@property (retain, nonatomic) UILabel *lblColor1;
@property (retain, nonatomic) UILabel *lblColor2;
@property (retain, nonatomic) UILabel *lblColor3;
@property (retain, nonatomic) UILabel *lblColor4;
@property(retain,nonatomic)UIColor*color;
-(void)SetCell:(Survey*)survey;
@end
