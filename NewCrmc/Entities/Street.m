//
//  Street.m
//  CRMC
//
//  Created by WebitMacMini Two on 6/17/14.
//  Copyright (c) 2014 Racheli Kleinhendler. All rights reserved.
//

#import "Street.h"

@implementation Street
- (id)init: (int)streetId StreetName:(NSString*)streetName CityId:(int)cityId
{
    self = [super init];
    if (self) {
        _streetId=streetId;
        _streetName=streetName;
        _cityId=cityId;
    }
    return self;
}
-(NSDictionary*)parseStreetToDict
{
    NSMutableDictionary* dict=[[NSMutableDictionary alloc]init];
    [dict setObject:[NSNumber numberWithInt:_streetId] forKey:@"streetId"];
    [dict setObject:_streetName forKey:@"streetName"];
    [dict setObject:[NSNumber numberWithInt:_cityId] forKey:@"cityId"];
     NSDictionary* dictStreet=[NSDictionary dictionaryWithObjectsAndKeys:dict,@"street", nil];
    return dictStreet;
}

-(NSMutableArray*)parseListStreetFromJson:(NSString*) strJson
{
    NSError* error;
    NSArray* arr=[NSJSONSerialization JSONObjectWithData:[strJson dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&error];
    NSMutableArray* arrStreet;
    if(arr.count>0)
    {
        arrStreet=[[NSMutableArray alloc]init];
        for (NSDictionary* dict in arr)
            [arrStreet addObject:[self parseStreetFromDict:dict]];
    }
    return  arrStreet;
}

-(Street*)parseStreetFromDict:(NSDictionary*)dict
{
    Street* street=[[Street alloc]init];
    street.streetId=(int)[[dict objectForKey:@"streetId"] integerValue];
    street.streetName=[dict objectForKey:@"streetName"];
    street.cityId=(int)[[dict objectForKey:@"cityId"] integerValue];
    return street;
}

@end
