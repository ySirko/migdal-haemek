//
//  News.m
//  CRMC
//
//  Created by WebitMacMini Two on 6/25/14.
//  Copyright (c) 2014 Racheli Kleinhendler. All rights reserved.
//

#import "News.h"

@implementation News
-(NSMutableArray*)parseListNewsFromJson:(NSString*) strJson
{
    NSError* error;
    NSArray* arr=[NSJSONSerialization JSONObjectWithData:[strJson dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&error];
    NSMutableArray* arrNews;
    if(arr.count>0)
    {
        arrNews=[[NSMutableArray alloc]init];
        for (NSDictionary* dict in arr)
            [arrNews addObject:[self parseNewsFromDict:dict]];
    }
    return  arrNews;
}

-(News*)parseNewsFromDict:(NSDictionary*)dict
{
    News* news=[[News alloc]init];
    news.newId=(int)[[dict objectForKey:@"newId"] integerValue];
    news.title=[dict objectForKey:@"title"];
    news.content=[dict objectForKey:@"content"];
    news.picture=[dict objectForKey:@"picture"];
    return news;
}

-(News*)parseNewsFromDictFeeds:(NSMutableDictionary*)dict
{
    News* news=[[News alloc]init];
    //   event.eventId=(int)[[dict objectForKey:@"eventId"] integerValue];
    //   event.notes=[dict objectForKey:@"notes"];
  //  news.eventDate=[dict objectForKey:@"pubDate"];
    news.title=[dict objectForKey:@"title"];
    //    event.address=[dict objectForKey:@"address"];
    news.content=[dict objectForKey:@"description"];
    //event.cityId=(int)[[dict objectForKey:@"cityId"] integerValue];
    // event.price=[dict objectForKey:@"price"];
 //   news.picture=[dict objectForKey:@"link"];
    return news;
}

@end
