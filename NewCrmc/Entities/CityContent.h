//
//  CityContent.h
//  חדרה
//
//  Created by Lior Ronen on 9/10/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CityContent : NSObject
@property(retain, nonatomic)NSString*aboutCity;
@property(retain, nonatomic)NSString*mayorSpeech;
@property(retain, nonatomic)NSString*emergencyTime;
@property(retain, nonatomic)NSString*cityHallWebsite;
@property(retain, nonatomic)NSString*facebook;
@property(retain, nonatomic)NSString*freedomOfInformation;
@property(retain, nonatomic)NSString*callCenter;
@property(retain,nonatomic)NSString*passwordToCrmcDB;
-(CityContent*)parseCityContentFromJson:(NSString*) strJson;
@end
