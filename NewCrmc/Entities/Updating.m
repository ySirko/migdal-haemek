//
//  Updating.m
//  CRMC
//
//  Created by WebitMacMini Two on 6/25/14.
//  Copyright (c) 2014 Racheli Kleinhendler. All rights reserved.
//

#import "Updating.h"

@implementation Updating
-(NSMutableArray*)parseListUpdatingFromJson:(NSString*) strJson
{
    NSError* error;
    NSArray* arr=[NSJSONSerialization JSONObjectWithData:[strJson dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&error];
    NSMutableArray* arrNews;
    if(arr.count>0)
    {
        arrNews=[[NSMutableArray alloc]init];
        for (NSDictionary* dict in arr)
            [arrNews addObject:[self parseUpdatingFromDict:dict]];
    }
    return  arrNews;
}

-(Updating*)parseUpdatingFromDict:(NSDictionary*)dict
{
    Updating* updating=[[Updating alloc]init];
    updating.UpdatingId=(int)[[dict objectForKey:@"newId"] integerValue];
    updating.title=[dict objectForKey:@"title"];
    updating.content=[dict objectForKey:@"content"];
    return updating;
}

@end
