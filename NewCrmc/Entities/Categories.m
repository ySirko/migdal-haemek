//
//  Categories.m
//  CRMC
//
//  Created by MyMac on 6/22/14.
//  Copyright (c) 2014 Racheli Kleinhendler. All rights reserved.
//

#import "Categories.h"
#import "NSData+Base64.h"
@implementation Categories
-(NSMutableArray*)parseListCategoriesFromJson:(NSString*) strJson
{
    NSError* error;
    NSArray* arr=[NSJSONSerialization JSONObjectWithData:[strJson dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&error];
    NSMutableArray* arrCategories;
    if(arr.count>0)
    {
        arrCategories=[[NSMutableArray alloc]init];
        for (NSDictionary* dict in arr)
            [arrCategories addObject:[self parseCategoriesFromDict:dict]];
    }
    return  arrCategories;
}

-(Categories*)parseCategoriesFromDict:(NSDictionary*)dict
{
    Categories* categories=[[Categories alloc]init];
    categories.CategoryId=(int)[[dict objectForKey:@"CategoryId"] integerValue];
    categories.categoryName=[dict objectForKey:@"categoryName"];
    categories.icon=[NSData dataFromBase64String:[NSString stringWithFormat:@"%@",[dict objectForKey:@"icon"]]];
    InterestPoint*interestPoint=[[InterestPoint alloc]init];
    categories.interestPoint=[interestPoint parseListInterestPointFromDict:[dict objectForKey:@"interestPoint"]Icon:categories.icon];
    return categories;
}

@end
