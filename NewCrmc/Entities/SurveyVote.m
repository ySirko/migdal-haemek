//
//  SurveyVote.m
//  NewCrmc
//
//  Created by WebitMacMini Two on 7/23/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import "SurveyVote.h"

@implementation SurveyVote
-(NSDictionary*)parseSurveyVoteToDict
{
    NSMutableDictionary* dict=[[NSMutableDictionary alloc]init];
    [dict setObject:[NSNumber numberWithInt:_selectedOptionId] forKey:@"selectedOptionId"];
    [dict setObject:[NSNumber numberWithInt:_age] forKey:@"age"];
    [dict setObject:[NSNumber numberWithInt:_gender] forKey:@"_gender"];
       NSDictionary* dictSurveyVote=[NSDictionary dictionaryWithObjectsAndKeys:dict,@"surveyVote", nil];
    return dictSurveyVote;
}
@end
