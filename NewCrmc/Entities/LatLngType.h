//
//  LatLngType.h
//  מגדל העמק
//
//  Created by MyMac on 12/10/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LatLngType : NSObject
@property int latLngTypeId;
@property(retain,nonatomic)UIColor*color;
-(NSMutableArray*)parseListLatLngTypeFromJson:(NSString*) strJson;
@end
