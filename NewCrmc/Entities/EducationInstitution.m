//
//  EducationInstitution.m
//  NewCrmc
//
//  Created by WebitMacMini Two on 7/21/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import "EducationInstitution.h"

@implementation EducationInstitution
-(NSMutableArray*)parseListEducationInstitutionFromJson:(NSString*) strJson
{
    NSError* error;
    NSArray* arr=[NSJSONSerialization JSONObjectWithData:[strJson dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&error];
    NSMutableArray* arrEducationInstitution;
    if(arr.count>0)
    {
        arrEducationInstitution=[[NSMutableArray alloc]init];
        for (NSDictionary* dict in arr)
            [arrEducationInstitution addObject:[self parseEducationInstitutionFromDict:dict]];
    }
    return  arrEducationInstitution;
}

-(EducationInstitution*)parseEducationInstitutionFromDict:(NSDictionary*)dict
{
    EducationInstitution* educationInstitution=[[EducationInstitution alloc]init];
    educationInstitution.educationInstitutionName=[dict objectForKey:@"educationInstitutionName"];
    educationInstitution.educationInstitutionId=(int)[[dict objectForKey:@"educationInstitutionId"] integerValue];
    educationInstitution.educationInstitutionToResidentId=(int)[[dict objectForKey:@"educationInstitutionToResidentId"] integerValue];
    return educationInstitution;
}
-(NSDictionary*)parseEducationInstitutionToDict
{
    NSMutableDictionary* dict=[[NSMutableDictionary alloc]init];
    [dict setObject:[NSNumber numberWithInt:_educationInstitutionId] forKey:@"educationInstitutionId"];
    [dict setObject:_educationInstitutionName forKey:@"educationInstitutionName"];
   // [dict setObject:[NSNumber numberWithInt:_educationInstitutionToResidentId] forKey:@"educationInstitutionToResidentId"];
    [dict setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"residentId"] forKey:@"educationInstitutionToResidentId"];
    

        return dict;
}
@end

