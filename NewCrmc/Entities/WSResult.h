//
//  WSResult.h
//  Global
//
//  Created by MyMac on 10/23/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WSResult : NSObject
@property(retain,nonatomic) NSArray* ResultObjects;
-(WSResult*)parseWSResultFromJson:(NSString*) strJson;
@end
