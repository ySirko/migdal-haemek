//
//  City.m
//  CRMC
//
//  Created by WebitMacMini Two on 6/16/14.
//  Copyright (c) 2014 Racheli Kleinhendler. All rights reserved.
//

#import "City.h"

@implementation City
- (id)init:(int) cityId Name:(NSString*)cityName
{
    self = [super init];
    if (self)
    {
        _cityId=cityId;
        _cityName=cityName;
    }
    return self;
}
-(NSDictionary*)parseCityToDict
{
    NSMutableDictionary* dict=[[NSMutableDictionary alloc]init];
    [dict setObject:[NSNumber numberWithInt:_cityId] forKey:@"cityId"];
    NSDictionary* dictCity=[NSDictionary dictionaryWithObjectsAndKeys:dict,@"city", nil];
    return dictCity;
}

-(NSMutableArray*)parseListCityFromJson:(NSString*) strJson
{
    NSError* error;
    NSArray* arr=[NSJSONSerialization JSONObjectWithData:[strJson dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&error];
    NSMutableArray* arrCity;
    if(arr.count>0)
    {
        arrCity=[[NSMutableArray alloc]init];
        for (NSDictionary* dict in arr)
            [arrCity addObject:[self parseCityFromDict:dict]];
    }
    return  arrCity;
}

-(City*)parseCityFromDict:(NSDictionary*)dict
{
    City* city=[[City alloc]init];
    city.cityId=(int)[[dict objectForKey:@"cityId"] integerValue];
    city.cityName=[dict objectForKey:@"cityName"];
    return city;
}

@end
