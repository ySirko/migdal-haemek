//
//  Report.h
//  CRMC
//
//  Created by WebitMacMini Two on 6/17/14.
//  Copyright (c) 2014 Racheli Kleinhendler. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Report : NSObject

@property int reportId;
@property int residentId;
@property(retain,nonatomic) NSString * descriptionn;
@property int  streetId;
@property (retain,nonatomic)NSString*  houseNumber;
@property(retain,nonatomic) NSString*  reportImage;
@property(retain,nonatomic) NSString* lon;
@property(retain,nonatomic) NSString* lat;
@property int statusId;
@property(retain,nonatomic) NSString* statusName;
@property(retain,nonatomic) NSString* streetName;
@property(retain,nonatomic) NSString* reportedDate;
- (id)init:(int)residentId Descriptionn:(NSString*)descriptionn StreetId:(int)streetId HouseNumber:(NSString*)houseNumber ReportImage:(NSString*)reportImage;
-(NSDictionary*)parseReportToDict;
-(NSMutableArray*)parseListReportFromJson:(NSString*) strJson;
-(Report*)parseReportFromDict:(NSDictionary*)dict;
@end
