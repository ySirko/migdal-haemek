//
//  WSDate.m
//  מגדל העמק
//
//  Created by Lior Ronen on 9/29/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import "WSDate.h"

@implementation WSDate
-(NSDictionary*)parseWSDateToDict
{
    NSMutableDictionary* dict=[[NSMutableDictionary alloc]init];
    [dict setObject:[NSString stringWithFormat:@"%d",_Day] forKey:@"Day"];
    [dict setObject:[NSString stringWithFormat:@"%d",_Month] forKey:@"Month"];
    [dict setObject:[NSString stringWithFormat:@"%d",_Year] forKey:@"Year"];
    NSDictionary* dictReport=[NSDictionary dictionaryWithObjectsAndKeys:dict,@"DateCreatedMobile", nil];
    return dict;
}
@end
