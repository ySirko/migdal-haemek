//
//  StreetListWrapper.m
//  Global
//
//  Created by MyMac on 10/23/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import "StreetListWrapper.h"

@implementation StreetListWrapper
-(NSDictionary*)parseStreetListWrapperToDict
{
    NSMutableDictionary* dict=[[NSMutableDictionary alloc]init];
    [dict setObject:[NSNumber numberWithInt:_clientId] forKey:@"clientId"];
    [dict setObject:_cityPassword forKey:@"password"];
    [dict setObject:@"Webit" forKey:@"userName"];
    NSDictionary* dictStreet=[NSDictionary dictionaryWithObjectsAndKeys:dict,@"GetStreetListWrapper", nil];
    return dictStreet;
}
@end
