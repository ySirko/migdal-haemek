//
//  Message.m
//  שוהם
//
//  Created by Lior Ronen on 12/15/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import "Message.h"

@implementation Message
-(NSMutableArray*)parseListMessageFromJson:(NSString*) strJson
{
    NSError* error;
    NSArray* arr=[NSJSONSerialization JSONObjectWithData:[strJson dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&error];
    NSMutableArray* arrMessages;
    if(arr.count>0)
    {
        arrMessages=[[NSMutableArray alloc]init];
        for (NSDictionary* dict in arr)
            [arrMessages addObject:[self parseMessageFromDict:dict]];
    }
    return  arrMessages;
}

-(Message*)parseMessageFromDict:(NSDictionary*)dict
{
    Message *m=[[Message alloc]init];
    m.messageToResidentId=(int)[[dict objectForKey:@"messageToResidentId"] integerValue];
    m.residentId=(int)[[dict objectForKey:@"residentId"] integerValue];
    m.message=[dict objectForKey:@"message"];
    return m;
}

@end
