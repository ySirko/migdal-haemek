//
//  NewTicketWrapper.m
//  Global
//
//  Created by MyMac on 10/23/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import "NewTicketWrapper.h"
#import "newTicket.h"
#import "WSAddress.h"
#import "WSPhoneNumber.h"
@implementation NewTicketWrapper
- (id)initWith :(NSString*)clientId Description:(NSString*)description AttachedFiles:(NSString*)attachedFiles StreetName:(NSString*)streetName HouseNumber:(NSString*)houseNumber Password:(NSString*)password
{
    self = [super init];
    if (self)
    {
        _clientId=clientId;
        _descriptionn=description;
        _streetName=streetName;
        _attachedFiles=attachedFiles;
        _houseNumber=houseNumber;
        _password=password;
    }
    return self;
}

-(NSDictionary*)parseNewTicketWrapperToDict
{
    NSMutableDictionary* dict=[[NSMutableDictionary alloc]init];
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    newTicket*new=[[newTicket alloc]initWith:_descriptionn AttachedFiles:_attachedFiles StreetName:_streetName HouseNumber:_houseNumber];
    if([defaults objectForKey:@"residentId"]!=nil)
    {
        [dict setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"residentId" ]forKey:@"ResidentId"];
        [dict setObject:[new parsenewTicketToDict] forKey:@"newTicket"];
    }
    else
    {
        [dict setObject:@"-1" forKey:@"ResidentId"];
        [dict setObject:[new ParseAnonymousNewTicketToDict] forKey:@"newTicket"];
    }
    [dict setObject:_clientId forKey:@"clientId"];
    [dict setObject:@"Webit" forKey:@"userName"];
    [dict setObject:_password forKey:@"password"];
    NSDictionary* dictreports=[NSDictionary dictionaryWithObjectsAndKeys:dict,@"NewTicketWrapper", nil];
    return dictreports;
}
-(NSDictionary*)parseTicketInfoWrapperToDict
{
    NSMutableDictionary* dict=[[NSMutableDictionary alloc]init];
    [dict setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"residentId" ]forKey:@"ResidentId"];
    [dict setObject:_clientId forKey:@"clientId"];
    [dict setObject:@"Webit" forKey:@"userName"];
    [dict setObject:_password forKey:@"password"];
    NSDictionary* dictreports=[NSDictionary dictionaryWithObjectsAndKeys:dict,@"GetTaskWrapper", nil];
    return dictreports;
}
@end
