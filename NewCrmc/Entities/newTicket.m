//
//  newTicket.m
//  מגדל העמק
//
//  Created by MyMac on 9/28/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import "newTicket.h"

@implementation newTicket
- (id)initWith:(NSString*)Description AttachedFiles:(NSString*)attachedFiles StreetName:(NSString*)streetName HouseNumber:(NSString*)houseNumber
{
    self = [super init];
    if (self)
    {
        _AttachedFiles=attachedFiles;
        _Descriptionn=Description;
        _ReporterFirstName=[[NSUserDefaults standardUserDefaults] objectForKey:@"firstName"];
        _ReporterLastName=[[NSUserDefaults standardUserDefaults] objectForKey:@"lastName"];
        _streetName=streetName;
        _houseNumber=houseNumber;
        _ReporterEmailAddress=[[NSUserDefaults standardUserDefaults] objectForKey:@"email"];
        _AttachedFiles=attachedFiles;
        _ReporterMobilePhoneNumber=[[WSPhoneNumber alloc]init];
        _ReporterHomePhoneNumber=[[WSPhoneNumber alloc]init];
        _ReporterHomePhoneNumber.PhoneNumber=[[NSUserDefaults standardUserDefaults] objectForKey:@"phoneNumber"];
        _ReporterMobilePhoneNumber.PhoneNumber=[[NSUserDefaults standardUserDefaults] objectForKey:@"mobileNumber"];
    }
    return self;
}
-(NSDictionary*)parsenewTicketToDict
{
    NSMutableDictionary* dictt=[[NSMutableDictionary alloc]init];
     [dictt setObject:_AttachedFiles forKey:@"AttachedFiles"];
    [dictt setObject:_Descriptionn forKey:@"Description"];
    WSAddress*add=[[WSAddress alloc]init];
    add.HouseNumber=_houseNumber;
    add.StreetName=_streetName;
     [dictt setObject:[add parseWSAddressToDict] forKey:@"Location"];
    [dictt setObject:[add parseWSAddressToDict] forKey:@"ReporterAddress"];
    [dictt setObject:_ReporterEmailAddress forKey:@"ReporterEmailAddress"];
    WSPhoneNumber*fax=[[WSPhoneNumber alloc]init];
    fax.PhoneNumber=@"4444444";
    [dictt setObject:[fax parseWSPhoneNumberToDict] forKey:@"ReporterFaxNumber"];
    [dictt setObject:_ReporterFirstName forKey:@"ReporterFirstName"];
    fax.PhoneNumber=_ReporterHomePhoneNumber.PhoneNumber;
    [dictt setObject:[fax parseWSPhoneNumberToDict] forKey:@"ReporterHomePhoneNumber"];
    [dictt setObject:_ReporterLastName forKey:@"ReporterLastName"];
    fax.PhoneNumber=_ReporterMobilePhoneNumber.PhoneNumber;
    [dictt setObject:[fax parseWSPhoneNumberToDict] forKey:@"ReporterMobilePhoneNumber"];
    return dictt;
}

-(NSDictionary*)ParseAnonymousNewTicketToDict
{
    NSMutableDictionary* dictt=[[NSMutableDictionary alloc]init];
    [dictt setObject:_AttachedFiles forKey:@"AttachedFiles"];
    [dictt setObject:_Descriptionn forKey:@"Description"];
    WSAddress*add=[[WSAddress alloc]init];
    add.HouseNumber=_houseNumber;
    add.StreetName=_streetName;
    [dictt setObject:[add parseWSAddressToDict] forKey:@"Location"];
    [dictt setObject:[add parseWSAddressToDict] forKey:@"ReporterAddress"];
    [dictt setObject:@"" forKey:@"ReporterEmailAddress"];
    WSPhoneNumber*fax=[[WSPhoneNumber alloc]init];
    fax.PhoneNumber=@"4444444";
    [dictt setObject:[fax parseWSPhoneNumberToDict] forKey:@"ReporterFaxNumber"];
    [dictt setObject:@"" forKey:@"ReporterFirstName"];
    fax.PhoneNumber=@"";
    [dictt setObject:[fax parseWSPhoneNumberToDict] forKey:@"ReporterHomePhoneNumber"];
    [dictt setObject:@"" forKey:@"ReporterLastName"];
    fax.PhoneNumber=@"";
    [dictt setObject:[fax parseWSPhoneNumberToDict] forKey:@"ReporterMobilePhoneNumber"];
    return dictt;
}
@end
