//
//  InterestPoint.m
//  CRMC
//
//  Created by MyMac on 6/22/14.
//  Copyright (c) 2014 Racheli Kleinhendler. All rights reserved.
//

#import "InterestPoint.h"

@implementation InterestPoint
-(InterestPoint*)parseInterestPointFromDict:(NSDictionary*)dict Icon:(NSData*)icon
{
    InterestPoint* interestPoint=[[InterestPoint alloc]init];
    interestPoint.interestPointId=(int)[[dict objectForKey:@"interestPointId"] integerValue];
    interestPoint.categoryId=(int)[[dict objectForKey:@"categoryId"] integerValue];
    interestPoint.cityId=(int)[[dict objectForKey:@"cityId"] integerValue];
    interestPoint.lon=[dict objectForKey:@"lon"];
    interestPoint.lat=[dict objectForKey:@"lat"];
    interestPoint.descriptionn=[dict objectForKey:@"description"];
    interestPoint.icon=icon;
    return interestPoint;
}
-(NSMutableArray*)parseListInterestPointFromDict:(NSDictionary*)dict Icon:(NSData*)icon
{
    NSMutableArray*arrInterestPoint=[[NSMutableArray alloc]init];
    for (NSDictionary* dic in dict)
        [arrInterestPoint addObject:[self parseInterestPointFromDict:dic Icon:icon]];
    return arrInterestPoint;
}
@end
