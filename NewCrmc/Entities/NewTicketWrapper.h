//
//  NewTicketWrapper.h
//  Global
//
//  Created by MyMac on 10/23/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NewTicketWrapper : NSObject
//@property (retain,nonatomic)NSString*ResidentId;
@property (retain,nonatomic)NSString*clientId;
@property (retain,nonatomic)NSString*descriptionn;
@property (retain,nonatomic)NSString*attachedFiles;
@property (retain,nonatomic)NSString*streetName;
@property (retain,nonatomic)NSString*houseNumber;
@property (retain,nonatomic)NSString*password;
-(NSDictionary*)parseNewTicketWrapperToDict;
- (id)initWith :(NSString*)clientId Description:(NSString*)description AttachedFiles:(NSString*)attachedFiles StreetName:(NSString*)streetName HouseNumber:(NSString*)houseNumber Password:(NSString*)password;
-(NSDictionary*)parseTicketInfoWrapperToDict;
@end
