//
//  WSPhoneNumber.h
//  מגדל העמק
//
//  Created by MyMac on 9/28/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WSPhoneNumber : NSObject
@property(retain,nonatomic) NSString* AreaCode;
@property(retain,nonatomic) NSString* PhoneNumber;
- (id)initwith:(NSString*)AreaCode phoneNumber:(NSString*)PhoneNumber;

-(NSDictionary*)parseWSPhoneNumberToDict;
@end
