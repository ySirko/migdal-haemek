//
//  InterestArea.h
//  NewCrmc
//
//  Created by WebitMacMini Two on 7/21/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InterestArea : NSObject
@property int interestAreaId;
@property(retain,nonatomic)NSString* interestAreaName;
@property int interestAreaToResidentId;
- (id)init:(int)interestAreaId InterestAreaName:(NSString*)interestAreaName InterestAreaToResidentId:(int)interestAreaToResidentId;
-(NSMutableArray*)parseListInterestAreaFromJson:(NSString*) strJson;
-(InterestArea*)parseInterestAreaFromDict:(NSDictionary*)dict;
-(NSDictionary*)parseInterestAreaToDict;
@end
