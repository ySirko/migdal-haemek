//
//  newTicket.h
//  מגדל העמק
//
//  Created by MyMac on 9/28/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WSPhoneNumber.h"
#import "WSAddress.h"
#import "WSDate.h"
@interface newTicket : NSObject

@property int CRMCTicketID ;
@property (retain,nonatomic)NSString *clientId;
@property (retain,nonatomic)NSString *residentID;
@property(retain,nonatomic)NSString* ReferenceID ;//0
@property(retain,nonatomic)WSDate* DateCreated;
@property(retain,nonatomic)NSString* Subject;
@property(retain,nonatomic)NSString* Descriptionn;//yes
@property(retain,nonatomic)NSString* DepartmentName;
@property(retain,nonatomic)WSAddress* Location;
@property(retain,nonatomic)NSString* Status;
@property(retain,nonatomic)NSString* ReporterFirstName;//yes
@property(retain,nonatomic)NSString* ReporterLastName;//yes
@property(retain,nonatomic)NSString* ReporterEmailAddress;
@property(retain,nonatomic)WSAddress* ReporterAddress;
@property(retain,nonatomic)WSPhoneNumber* ReporterHomePhoneNumber;
@property(retain,nonatomic)WSPhoneNumber* ReporterMobilePhoneNumber;
@property(retain,nonatomic)WSPhoneNumber* ReporterFaxNumber;
@property(retain,nonatomic)NSString* AttachedFiles;
@property(retain,nonatomic)NSString* ExtraParam1;
@property(retain,nonatomic)NSString* ExtraParam2;
@property(retain,nonatomic)NSString* houseNumber;
@property(retain,nonatomic)NSString* streetName;

-(NSDictionary*)parsenewTicketToDict;
-(NSDictionary*)ParseAnonymousNewTicketToDict;
- (id)initWith:(NSString*)Description AttachedFiles:(NSString*)attachedFiles StreetName:(NSString*)streetName HouseNumber:(NSString*)houseNumber;
//- (id)initWith:(NSString*)clientId referenceID:(NSString*)ReferenceID description:(NSString*)Description reporterFirstName:(NSString*)ReporterFirstName reporterLastName:(NSString*)ReporterLastName date:(WSDate*)DateCreated;

@end
