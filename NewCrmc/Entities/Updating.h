//
//  Updating.h
//  CRMC
//
//  Created by WebitMacMini Two on 6/25/14.
//  Copyright (c) 2014 Racheli Kleinhendler. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Updating : NSObject
@property int UpdatingId;
@property(retain,nonatomic)NSString* title;
@property(retain,nonatomic)NSString* content;
-(NSMutableArray*)parseListUpdatingFromJson:(NSString*) strJson;
-(Updating*)parseUpdatingFromDict:(NSDictionary*)dict;
@end
