//
//  Report.m
//  CRMC
//
//  Created by WebitMacMini Two on 6/17/14.
//  Copyright (c) 2014 Racheli Kleinhendler. All rights reserved.
//

#import "Report.h"
#import "NSData+Base64.h"
@implementation Report
- (id)init:(int)residentId Descriptionn:(NSString*)descriptionn StreetId:(int)streetId HouseNumber:(NSString*)houseNumber ReportImage:(NSString*)reportImage;
{
    self = [super init];
    if (self)
    {
        _residentId=residentId;
        _descriptionn=descriptionn;
        _streetId= streetId;
        _houseNumber=houseNumber;
        _reportImage=reportImage;
        _lat=@"";
        _lon=@"";
    }
    return self;
}
-(NSDictionary*)parseReportToDict
{
    NSMutableDictionary* dict=[[NSMutableDictionary alloc]init];
    [dict setObject:[NSNumber numberWithInt:_reportId] forKey:@"reportId"];
    [dict setObject:[NSNumber numberWithInt:_residentId] forKey:@"residentId"];
    [dict setObject:_descriptionn forKey:@"description"];
    [dict setObject:[NSNumber numberWithInt:_streetId] forKey:@"streetId"];
    [dict setObject:_houseNumber forKey:@"houseNumber"];
    [dict setObject:_reportImage forKey:@"reportImage"];
    [dict setObject:_lon forKey:@"lon"];
    [dict setObject:_lat forKey:@"lat"];
    NSDictionary* dictReport=[NSDictionary dictionaryWithObjectsAndKeys:dict,@"report", nil];
    return dictReport;
}

-(NSMutableArray*)parseListReportFromJson:(NSString*) strJson
{
    NSError* error;
    NSDictionary* arr=[NSJSONSerialization JSONObjectWithData:[strJson dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&error];
    NSMutableArray* arrReport;
    if(arr.count>0)
    {
        if([[arr valueForKey:@"ResultMessage"] isKindOfClass:[NSNull class]])
        {
            arr=[arr objectForKey:@"ResultObjects"];
            arrReport=[[NSMutableArray alloc]init];
            for (NSDictionary* dict in arr)
            [arrReport addObject:[self parseReportFromDict:dict]];
            return arrReport;
        }
        else
            return  nil;
    }
    return  nil;
}

-(Report*)parseReportFromDict:(NSDictionary*)dict
{
    Report* report=[[Report alloc]init];
    report.reportImage=[dict objectForKey:@"AttachedFiles"];
    report.descriptionn=[dict objectForKey:@"Description"];
    report.reportedDate=[dict objectForKey:@"DateCreatedClient"];
    report.reportId=(int)[[dict objectForKey:@"ReferenceID"] integerValue];
    report.statusName=[dict objectForKey:@"Status"];
    NSDictionary *arr=[dict objectForKey:@"Location"];
    report.houseNumber=[arr objectForKey:@"HouseNumber"];
    report.streetName=[arr objectForKey:@"StreetName"];
    return report;
}
@end
