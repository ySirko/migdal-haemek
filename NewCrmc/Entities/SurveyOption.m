//
//  SurveyOption.m
//  NewCrmc
//
//  Created by WebitMacMini Two on 7/23/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import "SurveyOption.h"

@implementation SurveyOption
-(NSMutableArray*)parseListSurveyOptionFromJson:(NSArray*) strJson
{
    NSMutableArray*arrSurveyOption=[[NSMutableArray alloc]init];
    for (NSDictionary* dict in strJson)
        [arrSurveyOption addObject:[self parseSurveyOptionFromDict:dict]];
    return  arrSurveyOption;
}

-(SurveyOption*)parseSurveyOptionFromDict:(NSDictionary*)dict
{
    SurveyOption* surveyOption=[[SurveyOption alloc]init];
    surveyOption.surveyOptionId=(int)[[dict objectForKey:@"surveyOptionId"] integerValue];
    surveyOption.surveyId=(int)[[dict objectForKey:@"surveyOptionId"] integerValue];
    surveyOption.optionId=(int)[[dict objectForKey:@"surveyOptionId"] integerValue];
    surveyOption.optionDescroption=[dict objectForKey:@"optionDescroption"];
    surveyOption.votePercent=(int)[[dict objectForKey:@"votePercent"] integerValue];
    surveyOption.image=[dict objectForKey:@"image"];
    return surveyOption;
}
@end
