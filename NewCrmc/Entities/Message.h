//
//  Message.h
//  שוהם
//
//  Created by Lior Ronen on 12/15/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Message : NSObject
@property int messageToResidentId;
@property int residentId;
@property (retain,nonatomic)NSString* message;
-(Message*)parseMessageFromDict:(NSDictionary*)dict;
-(NSMutableArray*)parseListMessageFromJson:(NSString*) strJson;
//-(News*)parseNewsFromDictFeeds:(NSMutableDictionary*)dict;

@end
