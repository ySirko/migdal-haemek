//
//  Survey.h
//  NewCrmc
//
//  Created by WebitMacMini Two on 7/23/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface Survey : NSObject
@property int surveyId;
@property(retain,nonatomic) NSString* surveyTitle;
@property(retain,nonatomic) NSArray* optionsList;
@property BOOL isActive;
-(NSMutableArray*)parseListSurveyFromJson:(NSString*) strJson;
-(Survey*)parseSurveyFromDict:(NSDictionary*)dict;
@end
