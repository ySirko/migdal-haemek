//
//  EducationInstitution.h
//  NewCrmc
//
//  Created by WebitMacMini Two on 7/21/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EducationInstitution : NSObject
@property int educationInstitutionId;
@property(retain,nonatomic)NSString*educationInstitutionName;
@property int educationInstitutionToResidentId;
-(NSMutableArray*)parseListEducationInstitutionFromJson:(NSString*) strJson;
-(EducationInstitution*)parseEducationInstitutionFromDict:(NSDictionary*)dict;
-(NSDictionary*)parseEducationInstitutionToDict;
@end
