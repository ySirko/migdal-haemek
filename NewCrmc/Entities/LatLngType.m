//
//  LatLngType.m
//  מגדל העמק
//
//  Created by MyMac on 12/10/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import "LatLngType.h"

@implementation LatLngType
-(NSMutableArray*)parseListLatLngTypeFromJson:(NSString*) strJson
{
    NSError* error;
    NSArray* arr=[NSJSONSerialization JSONObjectWithData:[strJson dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&error];
    NSMutableArray* arrCity;
    if(arr.count>0)
    {
        arrCity=[[NSMutableArray alloc]init];
        for (NSDictionary* dict in arr)
            [arrCity addObject:[self parseLatLngTypeFromDict:dict]];
    }
    return  arrCity;
}

-(LatLngType*)parseLatLngTypeFromDict:(NSDictionary*)dict
{
    LatLngType* latLngType=[[LatLngType alloc]init];
    latLngType.latLngTypeId=(int)[[dict objectForKey:@"latLngTypeId"] integerValue];
    latLngType.color=[self getUIColorObjectFromHexString:[dict objectForKey:@"color"] alpha:1];
    return latLngType;
}
- (UIColor *)getUIColorObjectFromHexString:(NSString *)hexStr alpha:(CGFloat)alpha
{
    unsigned int hexInt = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexStr];
    [scanner setCharactersToBeSkipped:[NSCharacterSet characterSetWithCharactersInString:@"#"]];
    [scanner scanHexInt:&hexInt];
    UIColor *color =
    [UIColor colorWithRed:((CGFloat) ((hexInt & 0xFF0000) >> 16))/255
                    green:((CGFloat) ((hexInt & 0xFF00) >> 8))/255
                     blue:((CGFloat) (hexInt & 0xFF))/255
                    alpha:alpha];
    return color;
}
@end
