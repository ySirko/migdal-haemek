//
//  WSAddress.m
//  מגדל העמק
//
//  Created by MyMac on 9/28/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import "WSAddress.h"

@implementation WSAddress
- (id)initwith :(NSString*)StreetName house:(NSString*)HouseNumber sitename:(NSString*)SiteName
{
   // self = [super init];
    if (self) {
        _HouseNumber=HouseNumber;
        _StreetName=StreetName;
        
    }
    return self;
}
-(NSDictionary*)parseWSAddressToDict
{
    NSMutableDictionary* dict=[[NSMutableDictionary alloc]init];
    [dict setObject:_StreetName forKey:@"StreetName"];
    [dict setObject:_HouseNumber forKey:@"HouseNumber"];
    return dict;
}
@end
