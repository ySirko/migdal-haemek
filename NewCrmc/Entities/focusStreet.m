//
//  focusStreet.m
//  מגדל העמק
//
//  Created by Lior Ronen on 9/29/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import "focusStreet.h"

@implementation focusStreet
-(NSDictionary*)parsefocusStreetToDict
{
    NSMutableDictionary* dict=[[NSMutableDictionary alloc]init];
    [dict setObject:_clientId forKey:@"clientId"];
    [dict setObject:_password forKey:@"password"];
    [dict setObject:_userName forKey:@"userName"];
    NSDictionary* dictReport=[NSDictionary dictionaryWithObjectsAndKeys:dict,@"GetStreetsListWrapper", nil];
    return dictReport;
}
-(focusStreet*)parseEntityFromDict:(NSDictionary*)dict
{
    focusStreet* street=[[focusStreet alloc]init];
    street.streetId=[dict objectForKey:@"StreetID"];
    street.streetName=[dict objectForKey:@"StreetName"];

    return street;
}

-(NSMutableArray*)parseListStreetsFromJson:(NSString*) strJson
{
    NSError* error;
    NSDictionary* dict=[NSJSONSerialization JSONObjectWithData:[strJson dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&error];

 //   NSArray* arr=[NSJSONSerialization JSONObjectWithData:[strJson dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&error];
  //  NSArray *obj=[NSJSONSerialization JSONObjectWithData:[arr dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&error];
NSArray *array= [dict objectForKey:@"ResultObjects"];
    NSMutableArray* arrStreet;
    if(array.count>0)
    {
        arrStreet=[[NSMutableArray alloc]init];
        for (NSDictionary* dict in array)
            [arrStreet addObject:[self parseEntityFromDict:dict]];
    }
    return  arrStreet;
}
@end
