//
//  SurveyOption.h
//  NewCrmc
//
//  Created by WebitMacMini Two on 7/23/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SurveyOption : NSObject
@property int surveyOptionId;
@property int surveyId;
@property int optionId;
@property(retain,nonatomic)NSString* optionDescroption;
@property int votePercent;
@property(retain,nonatomic)NSString* image;
-(NSMutableArray*)parseListSurveyOptionFromJson:(NSString*) strJson;
-(SurveyOption*)parseSurveyOptionFromDict:(NSDictionary*)dict;
@end
