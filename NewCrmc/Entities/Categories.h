//
//  Categories.h
//  CRMC
//
//  Created by MyMac on 6/22/14.
//  Copyright (c) 2014 Racheli Kleinhendler. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "InterestPoint.h"
@interface Categories : NSObject
@property int CategoryId;
@property(retain,nonatomic)NSString* categoryName;
@property(retain,nonatomic) NSArray* interestPoint;
@property(retain,nonatomic) NSData* icon;
-(NSMutableArray*)parseListCategoriesFromJson:(NSString*) strJson;
-(Categories*)parseCategoriesFromDict:(NSDictionary*)dict;
@end
