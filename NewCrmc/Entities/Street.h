//
//  Street.h
//  CRMC
//
//  Created by WebitMacMini Two on 6/17/14.
//  Copyright (c) 2014 Racheli Kleinhendler. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Street : NSObject
@property int streetId;
@property NSString* streetName;
@property int cityId;
- (id)init: (int)streetId StreetName:(NSString*)streetName CityId:(int)cityId;
-(NSDictionary*)parseStreetToDict;
-(NSMutableArray*)parseListStreetFromJson:(NSString*) strJson;
-(Street*)parseStreetFromDict:(NSDictionary*)dict;
@end
