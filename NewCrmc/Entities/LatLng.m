//
//  LatLng.m
//  מגדל העמק
//
//  Created by MyMac on 12/9/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import "LatLng.h"

@implementation LatLng

-(NSMutableArray*)parseListLatLngFromJson:(NSArray*) strJson
{
    NSMutableArray*arrSurveyOption=[[NSMutableArray alloc]init];
    for (NSDictionary* dict in strJson)
        [arrSurveyOption addObject:[self parseLatLngFromDict:dict]];
    return  arrSurveyOption;
}
-(LatLng*)parseLatLngFromDict:(NSDictionary*)dict
{
    LatLng* latLng=[[LatLng alloc]init];
    latLng.nvLatLangInfo=[dict objectForKey:@"latLangInfo"];
    latLng.iLatLngType=(int)[[dict objectForKey:@"latLngType"] integerValue];
    latLng.nvLat=[dict objectForKey:@"lat"];
    latLng.nvLng=[dict objectForKey:@"lng"];
    return latLng;
}
@end
