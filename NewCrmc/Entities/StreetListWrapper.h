//
//  StreetListWrapper.h
//  Global
//
//  Created by MyMac on 10/23/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StreetListWrapper : NSObject
@property int clientId;
@property(retain,nonatomic)NSString*password;
@property(retain,nonatomic)NSString*userName;
@property(retain,nonatomic)NSString*cityPassword;
-(NSDictionary*)parseStreetListWrapperToDict;
@end
