//
//  WebViewController.m
//  NewCrmc
//
//  Created by MyMac on 7/31/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import "WebViewController.h"
#import "Generic.h"
@interface WebViewController ()
{
    Generic*generic;
}
@end

@implementation WebViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    generic=[[Generic alloc]init];
    self.Title.text=self.header;
    self.lblBackgroundTitle.backgroundColor=_color;
    _web=[[UIWebView alloc]initWithFrame:CGRectMake(0, 132, 320, self.view.frame.size.height-140)];
    _web.scalesPageToFit = YES;
    _web.delegate=self;
    if(_IsHtml)
    {
        [generic showNativeActivityIndicator:self];
        [_web loadHTMLString:_nsurl baseURL:nil];
    }
    else
    {
        NSString *encodedUrl =[_nsurl stringByRemovingPercentEncoding];
        if (![encodedUrl containsString:@"#"]) {
            encodedUrl = [encodedUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        }
        NSURL*url=[NSURL URLWithString:encodedUrl];
        NSLog(@"Current URL: %@",url);
        NSURLRequest *request=[NSURLRequest requestWithURL:url];
        [_web loadRequest:request];
        _web.scalesPageToFit = YES;
    }
    [self.view addSubview:_web];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [generic hideNativeActivityIndicator:self];
    NSLog(@"Current URL = %@",webView.request.URL);
}
@end
