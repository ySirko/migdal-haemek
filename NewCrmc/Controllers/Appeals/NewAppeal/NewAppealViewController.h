//
//  NewAppealViewController.h
//  Crmc
//
//  Created by MyMac on 5/5/14.
//  Copyright (c) 2014 nbsh. All rights reserved.
//

#import "ViewController.h"
#import "SubViewController.h"

@interface NewAppealViewController : SubViewController <UITableViewDataSource,UINavigationControllerDelegate, UITableViewDelegate, UISearchDisplayDelegate,UIAlertViewDelegate, UISearchBarDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UITextFieldDelegate,UITextViewDelegate>
@property (retain, nonatomic) UILabel*lblbackgroung;
@property (retain, nonatomic) UITextField *txtNumHouse;
@property (retain, nonatomic) UITextField *txtFildCity;
@property (retain, nonatomic) UITextField *txtFildAddress;
@property (retain, nonatomic) UITextView *txtViewDescription;
@property (retain, nonatomic) UIImageView *imgView;
@property (retain, nonatomic) UIImageView *imgCamera;
@property (retain, nonatomic) UIButton *btnImag_click;
@property (retain, nonatomic) UIButton *btnSend;
@property(nonatomic,retain)NSArray * listStreets;
@property (retain, nonatomic)UIColor*color;
@end
