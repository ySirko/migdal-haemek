//
//  MyAppealCell.h
//  CRMC
//
//  Created by Lior Ronen on 5/12/14.
//  Copyright (c) 2014 neomi&batya&shuli. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Report.h"
@interface MyAppealCell : UITableViewCell<UITextViewDelegate>
@property (retain, nonatomic) UILabel *lblBackGround;
@property (retain, nonatomic) UIImageView *imgDescription;
@property (retain, nonatomic) UIImageView *imgAddress;
@property (retain, nonatomic) UILabel *lblDate;
@property (retain, nonatomic) UILabel *lblStreet;
@property (retain, nonatomic) UITextView *tvDescription;
@property (retain, nonatomic) UILabel *lblStatus;
@property (retain, nonatomic) UILabel *lblNumAppeal;
@property (retain, nonatomic) UIImageView *imgAppeal;
@property (retain, nonatomic) UIImageView *imgCamera;
@property (retain, nonatomic) UILabel *lblNoPhoto;
@property (retain, nonatomic)UIColor*color;
-(void) setCell :(Report*)myAppeal;
@end
