//
//  MyAppealCell.m
//  CRMC
//
//  Created by Lior Ronen on 5/12/14.
//  Copyright (c) 2014 neomi&batya&shuli. All rights reserved.
//

#import "MyAppealCell.h"
#import "NSData+Base64.h"
@implementation MyAppealCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}
-(void) setCell :(Report*)myAppeal
{
    _lblBackGround=[[UILabel alloc]initWithFrame:CGRectMake(98, 10, 201, 115)];
    _lblBackGround.layer.borderColor=_color.CGColor;
    _lblBackGround.layer.borderWidth=1;
    _lblBackGround.backgroundColor=[UIColor whiteColor];
    [self addSubview:_lblBackGround];
    _imgDescription=[[UIImageView alloc]initWithFrame:CGRectMake(273, 63, 17, 17)];
    _imgDescription.image=[UIImage imageNamed:@"Comment.png"];
    [self addSubview:_imgDescription];
    _imgAddress=[[UIImageView alloc]initWithFrame:CGRectMake(276, 40, 14, 16)];
    _imgAddress.image=[UIImage imageNamed:@"Address.png"];
    [self addSubview:_imgAddress];
    _lblDate=[[UILabel alloc]initWithFrame:CGRectMake(98, 14, 169, 22)];
    _lblDate.text=myAppeal.reportedDate;
    _lblDate.textAlignment=NSTextAlignmentRight;
     _lblDate.font = [UIFont fontWithName:@"Helvetica Neue" size:11.0];
    [self addSubview:_lblDate];
    _lblStreet=[[UILabel alloc]initWithFrame:CGRectMake(98, 37, 169, 21)];
    _lblStreet.text=[NSString stringWithFormat:@"%@ %@" ,myAppeal.streetName,myAppeal.houseNumber];
    _lblStreet.textAlignment=NSTextAlignmentRight;
    _lblStreet.font = [UIFont fontWithName:@"Helvetica Neue" size:11.0];
    [self addSubview:_lblStreet];
    _tvDescription=[[UITextView alloc]initWithFrame:CGRectMake(105, 56, 169, 60)];
     _tvDescription.text=myAppeal.descriptionn;
    _tvDescription.textAlignment=NSTextAlignmentRight;
    _tvDescription.font = [UIFont fontWithName:@"Helvetica Neue" size:11.0];
    [self addSubview:_tvDescription];
    _lblStatus=[[UILabel alloc]initWithFrame:CGRectMake(108, 123, 190, 21)];
    _lblStatus.text=[NSString stringWithFormat:@" סטטוס: %@",myAppeal.statusName];
    _lblStatus.font = [UIFont fontWithName:@"Helvetica-Bold" size:13.0];
    _lblStatus.textAlignment=NSTextAlignmentRight;
    [self addSubview:_lblStatus];
    _lblNumAppeal=[[UILabel alloc]initWithFrame:CGRectMake(16, 123, 100, 21)];
    _lblNumAppeal.text=[NSString stringWithFormat:@" מס'פניה: %d",myAppeal.reportId];
    _lblNumAppeal.font = [UIFont fontWithName:@"Helvetica-Bold" size:13.0];
    _lblNumAppeal.textAlignment=NSTextAlignmentLeft;
    [self addSubview:_lblNumAppeal];
    
    _imgAppeal=[[UIImageView alloc]initWithFrame:CGRectMake(13, 10, 78, 115)];
    _imgAppeal.layer.borderWidth=1;
    _imgAppeal.backgroundColor=[UIColor whiteColor];
    [self addSubview:_imgAppeal];
    _imgCamera=[[UIImageView alloc]initWithFrame:CGRectMake(25, 24, 54, 41)];
    _imgCamera.image=[UIImage imageNamed:@"Camera.png"];
    [self addSubview:_imgCamera];
    _lblNoPhoto=[[UILabel alloc]initWithFrame:CGRectMake(22, 63, 61, 35)];
    _lblNoPhoto.text=@"לא צורפה תמונה לדיווח";
    _lblNoPhoto.textAlignment=NSTextAlignmentCenter;
    _lblNoPhoto.font = [UIFont fontWithName:@"Helvetica Neue" size:12.0];
    _lblNoPhoto.numberOfLines=2;
    [self addSubview:_lblNoPhoto];
    NSData* data=  [NSData dataFromBase64String:[NSString stringWithFormat:@"%@",myAppeal.reportImage]];
    if(![myAppeal.reportImage isKindOfClass:[NSNull class]])
    {
       _lblNoPhoto.hidden=YES;
       _imgCamera.hidden=YES;
       [_imgAppeal setImage:[UIImage imageWithData:data]];
    }
     _imgAppeal.layer.borderColor=_color.CGColor;
}
@end
