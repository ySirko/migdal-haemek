//
//  MyAppealTableViewController.m
//  CRMC
//
//  Created by Lior Ronen on 5/12/14.
//  Copyright (c) 2014 neomi&batya&shuli. All rights reserved.
//

#import "MyAppealTableViewController.h"
#import "Resident.h"
#import "NewTicketWrapper.h"
@interface MyAppealTableViewController ()
@end

@implementation MyAppealTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor=[UIColor whiteColor];
    self.lblBackgroundTitle.backgroundColor=_color;
    self.Title.text=self.header;
    _tbvAppeals=[[UITableView alloc]init];
    _tbvAppeals.frame=CGRectMake(0,157, 320, self.view.frame.size.height-180);
    _tbvAppeals.backgroundColor=[UIColor colorWithRed:23.0f/255.0f green:184.0f/255.0f blue:75.0f/255.0f alpha:0.6f];
    _tbvAppeals.dataSource=self;
    _tbvAppeals.delegate=self;
    _tbvAppeals.separatorColor = [UIColor clearColor];
    [self.view addSubview:_tbvAppeals];
    _noapeals=[[UILabel alloc]initWithFrame:CGRectMake(20, 180, self.view.frame.size.width-40, 50)];
    _noapeals.textColor=[UIColor whiteColor];
    _noapeals.numberOfLines=2;
    _noapeals.textAlignment=NSTextAlignmentCenter;
    [self.view addSubview:_noapeals];
    Resident*resident=[[Resident alloc]init];
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    if(!([defaults objectForKey:@"residentId"]==nil))
    {
        resident.residentId=(int)[[[NSUserDefaults standardUserDefaults] objectForKey:@"residentId"] integerValue];
        resident.firstName=[[NSUserDefaults standardUserDefaults] objectForKey:@"firstName"];
        resident.houseNumber=[[NSUserDefaults standardUserDefaults] objectForKey:@"houseNumber"];
        resident.email=[[NSUserDefaults standardUserDefaults] objectForKey:@"email"];
        resident.lastName=[[NSUserDefaults standardUserDefaults] objectForKey:@"lastName"];
        resident.mobileNumber=[[NSUserDefaults standardUserDefaults] objectForKey:@"mobileNumber"];
        resident.phoneNumber=[[NSUserDefaults standardUserDefaults] objectForKey:@"phoneNumber"];
        resident.streetId=(int)[[[NSUserDefaults standardUserDefaults] objectForKey:@"streetId"] integerValue];
        [self.generic showNativeActivityIndicator:self];
        NewTicketWrapper *new=[[NewTicketWrapper alloc]init];
        new.password=self.appDelegate.cityContent.passwordToCrmcDB;
        new.clientId=[NSString stringWithFormat:@"%d",self.appDelegate.city.cityId];
        [self.connection connectionToService:@"getTask" jsonDictionary:[new parseTicketInfoWrapperToDict] controller:self withSelector:@selector(GetTicketInfoWrapperResult:)];
    }
    else
        _noapeals.text=@"מאחר ואינך משתמש רשום לא ניתן להציג את הפניות שנשלחו";
}

-(void)GetTicketInfoWrapperResult:(NSString*)result
{
    [self.generic hideNativeActivityIndicator:self];
    if(![result isEqualToString:@""]&&![result isEqualToString:@"[]"])
    {
        Report * report=[[Report alloc]init];
        _listMyApeals=[[NSArray alloc]initWithArray:[report parseListReportFromJson:result]];
        if(_listMyApeals.count>0)
            [_tbvAppeals reloadData];
        else
            _noapeals.text=@"לא נשלחו פניות";
    }
    else
        NSLog(@"Empty reports list");
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _listMyApeals.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 150;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    MyAppealCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell = [[MyAppealCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    cell.color=[[UIColor alloc]init];
    cell.color=_color;
    [cell setCell:_listMyApeals[indexPath.row]];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.backgroundColor=[UIColor clearColor];
    return cell;
}
@end
