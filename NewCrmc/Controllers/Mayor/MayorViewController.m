//
//  MayorViewController.m
//  CRMC
//
//  Created by WebitMacMini Two on 6/18/14.
//  Copyright (c) 2014 Racheli Kleinhendler. All rights reserved.
//

#import "MayorViewController.h"

@interface MayorViewController ()

@end

@implementation MayorViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.lblBackgroundTitle.backgroundColor=_color;
    self.Title.text=self.header;
    [self setDynamicObject];
   _txtview.delegate=self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)setDynamicObject
{
    _txtview=[[UITextView alloc]initWithFrame:CGRectMake(10, 163, 300, self.view.frame.size.height-181)];
    _txtview.text=_mayorSpeech;
    
    _txtview.font=[UIFont fontWithName:@"Helvetica" size:13];
#warning Comment
    //if(self.appDelegate.city.cityId==1)
    _txtview.textAlignment=NSTextAlignmentRight;
    //else
    //    _txtview.textAlignment=NSTextAlignmentJustified;
    
    [self.view addSubview:_txtview];
    
    UILabel *lblUpLine=[[UILabel alloc]initWithFrame:CGRectMake(0, 157, 320, 6)];
    lblUpLine.backgroundColor=_color;
    [self.view addSubview:lblUpLine];
    
    UILabel *lblDownLine=[[UILabel alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height-18, 320, 6)];
    lblDownLine.backgroundColor=_color;
    [self.view addSubview:lblDownLine];
    
    switch (_index)
    {
        case 10:
            if(self.appDelegate.cityContent.mayorSpeech)
                _txtview.text=[[NSString alloc]initWithString: self.appDelegate.cityContent.mayorSpeech];
            else
                _txtview.text=@"";
            break;
        case 11:
       
            if(self.appDelegate.cityContent.aboutCity)
            {
                //////////////////////
                _txtview.text=[[NSString alloc]initWithString: self.appDelegate.cityContent.aboutCity];
                MayorViewController *mayor=self;
                mayor.imgMayor=[[UIImageView alloc]initWithFrame:CGRectMake(10, 220, 140, 140)];
                mayor.imgMayor.image=[UIImage imageNamed:@"imageCity.jpg"];
                mayor.imgMayor.contentMode = UIViewContentModeScaleAspectFit;
                [mayor.txtview addSubview:mayor.imgMayor];

                
        }
            else
                _txtview.text=@"";
            break;
        case 12:
            if(self.appDelegate.cityContent.emergencyTime)
                _txtview.text=[[NSString alloc]initWithString: self.appDelegate.cityContent.emergencyTime];
            else
                _txtview.text=@"";
            break;
            break;
        case 20:
            if(self.appDelegate.cityContent.freedomOfInformation)
            {  _txtview.text=[[NSString alloc]initWithString: self.appDelegate.cityContent.freedomOfInformation];
            
            //            if(self.appDelegate.cityContent.aboutCity)
            //                _txtview.text=[[NSString alloc]initWithString: self.appDelegate.cityContent.aboutCity];
        }
            else
                _txtview.text=@"";
            break;

        default:
            break;
    }
}
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    return NO;
}
//- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
//    
//    if([text isEqualToString:@"\n"]) {
//        [textView resignFirstResponder];
//        return NO;
//    }
//    
//    return YES;
//}
//-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
//{
////    if(flag)
////    {
//        [UIView beginAnimations:nil context:nil];
//        [UIView setAnimationDuration:0.5];
//        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y+150, self.view.frame.size.width, self.view.frame.size.height);
//        [UIView commitAnimations];
//        [self.view endEditing:YES];
//      //  flag=NO;
////    }
//}

@end