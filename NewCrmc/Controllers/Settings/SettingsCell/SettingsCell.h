//
//  SettingsCell.h
//  NewCrmc
//
//  Created by WebitMacMini Two on 7/22/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol SettingsCellDelegate <NSObject>
-(void)SettingsCellDelegateClick:(int) index ischeck:(BOOL) isCheck;

@end

@interface SettingsCell : UITableViewCell<SettingsCellDelegate>
@property (retain,nonatomic) id<SettingsCellDelegate> delegate;
@property (retain, nonatomic) UILabel *lblBackgroung;
@property (retain, nonatomic) UILabel *lbl;
@property (retain, nonatomic) UIButton *btnSwitch;
@property (retain, nonatomic)UIButton *btnSwitchBack;
@property BOOL isCheck;
@property int index;
@end
