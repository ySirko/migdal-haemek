//
//  SettingsViewController.m
//  NewCrmc
//
//  Created by WebitMacMini Two on 7/21/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import "SettingsViewController.h"
#import "SettingsCell.h"
#import "InterestArea.h"
#import "EducationInstitution.h"
#import "Resident.h"
#import "HomeViewController.h"
@interface SettingsViewController ()
{
    NSMutableArray*choose;
    Resident* resident;
 //   int indexx;
}
@end

@implementation SettingsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
   // indexx=0;
    choose=[[NSMutableArray alloc]init];
    _tbl=[[UITableView alloc]initWithFrame:CGRectMake(20, 136, 280, 250)];
    _tbl.dataSource=self;
    _tbl.delegate=self;
    [self.view addSubview:_tbl];
   _lblTitleQuestion=[[UILabel alloc]initWithFrame:CGRectMake(40, 112, 243, 21)];
    _lblTitleQuestion.textColor=[UIColor whiteColor];
    _lblTitleQuestion.textAlignment=NSTextAlignmentRight;
    _lblTitleQuestion.font=[UIFont fontWithName:@"" size:17];
    [self.view addSubview:_lblTitleQuestion];
    
    _btnOK=[[UIButton alloc]initWithFrame:CGRectMake(20, 384, 280, 27)];
    _btnOK.backgroundColor=[UIColor colorWithRed:9.0f/255.0f green:134.0f/255.0f blue:134.0f/255.0f alpha:1.0f];
    [self.view addSubview:_btnOK];
    [_btnOK setTitle:@"אישור" forState:normal];
    [_btnOK addTarget:self action:@selector(btnNextClick:) forControlEvents:UIControlEventTouchUpInside];
    
    resident=[[Resident alloc]init];
    [self.generic showNativeActivityIndicator:self];
    if(_isInterestArea==1)
    {
        _lblTitleQuestion.text=@"אילו תחומי ענין מענינים אותך?";
    [self.connection connectionToService:@"GetInterestAreaToCity" jsonDictionary:[self.appDelegate.city parseCityToDict] controller:self withSelector:@selector(GetInterestAreaToCityResult:)];
    }
    else
    {
        _lblTitleQuestion.text=@"מאילו מוסדות לימוד הינך מעונין לקבל הודעות?";
         [_lblTitleQuestion setFont:[UIFont systemFontOfSize:13]];
        [self.connection connectionToService:@"GetEducationInstitutionToCity" jsonDictionary:[self.appDelegate.city parseCityToDict] controller:self withSelector:@selector(GetEducationInstitutionToCityResult:)];
    }
    _tbl.backgroundColor =[UIColor clearColor];
    _tbl.separatorColor=[UIColor clearColor];
}

-(void)GetInterestAreaToCityResult:(NSString*)result
{
    if(![result isEqualToString:@""]&&![result isEqualToString:@"[]"])
    {
        InterestArea * interestArea=[[InterestArea alloc]init];
        _list=[[NSArray alloc]initWithArray:[interestArea parseListInterestAreaFromJson:result]];
        resident.residentId=[[[NSUserDefaults standardUserDefaults]objectForKey:@"residentId"]integerValue];
        resident.cityId=self.appDelegate.city.cityId;
        [self.connection connectionToService:@"GetInterestAreasToResident" jsonDictionary:[resident parseResidentIdToDict] controller:self withSelector:@selector(GetInterestAreasToResidentResult:)];
    }
    else
    {
        NSLog(@"Empty reports list");
        [self.generic hideNativeActivityIndicator:self];
    }
}
-(void)GetInterestAreasToResidentResult:(NSString*)result
{
    [self.generic hideNativeActivityIndicator:self];
    if(![result isEqualToString:@""]&&![result isEqualToString:@"[]"])
    {
        InterestArea * interestArea=[[InterestArea alloc]init];
        choose=[[NSMutableArray alloc]initWithArray:[interestArea parseListInterestAreaFromJson:result]];
    }
    else
        NSLog(@"Empty interestArea list");
   // indexx=0;
    [_tbl reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
       //  indexx=0;
    return _list.count;

}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
   
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
       static NSString *CellIdentifier = @"Cell";
    SettingsCell *cell ;
        if (cell == nil)
    {

        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        cell = [[SettingsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.lblBackgroung=[[UILabel alloc]initWithFrame:CGRectMake(0, 3, 280, 40)];
        cell.lblBackgroung.backgroundColor=[UIColor whiteColor];
        [cell addSubview:cell.lblBackgroung];
        NSArray* filterArray;
        cell.lbl=[[UILabel alloc]initWithFrame:CGRectMake(116, 12, 149, 26)];
        [cell addSubview:cell.lbl];
        cell.lbl.layer.cornerRadius=2;
        cell.lbl.textAlignment = NSTextAlignmentRight;
        cell.btnSwitchBack=[[UIButton alloc]initWithFrame:CGRectMake(17,6,33,33)];
        [cell addSubview:cell.btnSwitchBack];
        cell.btnSwitch=[[UIButton alloc]initWithFrame:CGRectMake(20,9,27,27)];
        [cell addSubview:cell.btnSwitch];
        [cell.btnSwitch addTarget:cell action:@selector(btnSwichClick:) forControlEvents:UIControlEventTouchUpInside];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        if([NSStringFromClass([_list[indexPath.row]class])isEqualToString:@"EducationInstitution"])
        {
            cell.lbl.text=((EducationInstitution*)_list[indexPath.row]).educationInstitutionName;
            filterArray = [choose filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"educationInstitutionId =[cd] %d",((EducationInstitution*)_list[indexPath.row]).educationInstitutionId]];
        }
        else
        {
            cell.lbl.text=((InterestArea*)_list[indexPath.row]).interestAreaName;
            filterArray = [choose filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"interestAreaId =[cd] %d",((InterestArea*)_list[indexPath.row]).interestAreaId]];
        }
        BOOL c=((SettingsCell*)cell).isCheck;
        if(filterArray.count>0)
            cell.btnSwitch.backgroundColor=[UIColor colorWithRed:9.0f/255.0f green:134.0f/255.0f blue:134.0f/255.0f alpha:1.0f];
        else
            cell.btnSwitch.backgroundColor=[UIColor clearColor];
    }

//    cell.lblBackgroung=[[UILabel alloc]initWithFrame:CGRectMake(0, 3, 280, 40)];
//    cell.lblBackgroung.backgroundColor=[UIColor whiteColor];
//    [cell addSubview:cell.lblBackgroung];
//    NSArray* filterArray;
//    cell.lbl=[[UILabel alloc]initWithFrame:CGRectMake(116, 12, 149, 26)];
//    [cell addSubview:cell.lbl];
//    cell.lbl.layer.cornerRadius=2;
//    cell.lbl.textAlignment = NSTextAlignmentRight;
//    cell.btnSwitchBack=[[UIButton alloc]initWithFrame:CGRectMake(17,6,33,33)];
//    [cell addSubview:cell.btnSwitchBack];
//    cell.btnSwitch=[[UIButton alloc]initWithFrame:CGRectMake(20,9,27,27)];
//    [cell addSubview:cell.btnSwitch];
//    [cell.btnSwitch addTarget:cell action:@selector(btnSwichClick:) forControlEvents:UIControlEventTouchUpInside];
//    cell.selectionStyle=UITableViewCellSelectionStyleNone;
//    if([NSStringFromClass([_list[indexPath.row]class])isEqualToString:@"EducationInstitution"])
//    {
//        cell.lbl.text=((EducationInstitution*)_list[indexPath.row]).educationInstitutionName;
//        filterArray = [choose filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"educationInstitutionId =[cd] %d",((EducationInstitution*)_list[indexPath.row]).educationInstitutionId]];
//    }
//    else
//    {
//        cell.lbl.text=((InterestArea*)_list[indexPath.row]).interestAreaName;
//        filterArray = [choose filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"interestAreaId =[cd] %d",((InterestArea*)_list[indexPath.row]).interestAreaId]];
//    }
//    BOOL c=((SettingsCell*)cell).isCheck;
//    if(filterArray.count>0)
//        cell.btnSwitch.backgroundColor=[UIColor colorWithRed:9.0f/255.0f green:134.0f/255.0f blue:134.0f/255.0f alpha:1.0f];
//    else
//        cell.btnSwitch.backgroundColor=[UIColor clearColor];
    cell.delegate=self;
    cell.btnSwitchBack.layer.borderColor=[UIColor colorWithRed:9.0f/255.0f green:134.0f/255.0f blue:134.0f/255.0f alpha:1.0f].CGColor;
    cell.btnSwitchBack.layer.borderWidth=0.5f;
    cell.backgroundColor=[UIColor clearColor];
    cell.btnSwitch.layer.cornerRadius=14;
    cell.btnSwitchBack.layer.cornerRadius=17;
    cell.index=indexPath.row;
   // indexx++;
    return cell;
}
- (IBAction)btnNextClick:(id)sender
{
    NSMutableArray* listArea=[[NSMutableArray alloc]init];

    Resident*resident1=[[Resident alloc]init];
    resident1.residentId=(int)[[[NSUserDefaults standardUserDefaults] objectForKey:@"residentId"] integerValue];
    [self.generic showNativeActivityIndicator:self];
    if([NSStringFromClass([_list[0]class])isEqualToString:@"InterestArea"])
    {
//        resident1.interestAreasList=[[NSMutableArray alloc]init];
        for(int i=0;i<_list.count;i++)
        {
//            NSIndexPath* ind=[[NSIndexPath alloc]init];
          NSNumber* inde=[NSNumber numberWithInt:i];
//            ind=[[NSIndexPath alloc]initWithIndex:[inde integerValue]];
//            //ind.row=inde;
            NSArray*filt=[[NSArray alloc]init];
             filt = [choose filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"interestAreaId =[cd] %d",((InterestArea*)_list[i]).interestAreaId]];
            if(filt.count>0)
            {
                [resident1.interestAreasList addObject:_list[i]];
                [listArea addObject:_list[i]];
            }
//          NSIndexPath*  indexPath = [NSIndexPath indexPathForRow:[inde integerValue] inSection:0];
//                      if((((SettingsCell*)[_tbl cellForRowAtIndexPath:indexPath]).btnSwitch.backgroundColor==[UIColor colorWithRed:9.0f/255.0f green:134.0f/255.0f blue:134.0f/255.0f alpha:1.0f]))
//                      {   [resident1.interestAreasList addObject:_list[i]];
//                          [listArea addObject:_list[i]];
// }
//                      else{
//                                              }
        }
    
        resident1.interestAreasList=[[NSMutableArray alloc]init];
        resident1.interestAreasList=listArea;
                resident1.residentId=[[[NSUserDefaults standardUserDefaults]objectForKey:@"residentId"]integerValue];
        [self.connection connectionToService:@"SetInterestAreasToResident" jsonDictionary:[resident1 parseResidentToDictSettingInterestAreas] controller:self withSelector:@selector(SetInterestAreasToResidentResult:)];
    }
    else
    {
        resident1.EducationInstitutionsList=[[NSMutableArray alloc]init];
        for(int i=0;i<_list.count;i++)
            if(!(((SettingsCell*)[_tbl visibleCells][i]).btnSwitch.backgroundColor==[UIColor clearColor]))
                [resident1.EducationInstitutionsList addObject:_list[i]];
        [self.connection connectionToService:@"SetEducationInstitutionsToResident" jsonDictionary:[resident1 parseResidentToDictSettingEducationInstitutions] controller:self withSelector:@selector(SetEducationInstitutionsToResidentResult:)];
    }
}
-(void)SetInterestAreasToResidentResult:(NSString*)result
{
    [self.generic hideNativeActivityIndicator:self];
    if([result isEqualToString:@"true"])
    {
        [UIView animateWithDuration:0.5
                              delay:.2f
                            options: UIViewAnimationOptionAllowAnimatedContent
                         animations:^{ [self.view setAlpha:0];
                         }
                         completion:^(BOOL finished){
                             [self.view removeFromSuperview];
                         }];
    }
    else
        NSLog(@"Empty reports list");
}
-(void)GetEducationInstitutionToCityResult:(NSString*)result
{
    if(![result isEqualToString:@""]&&![result isEqualToString:@"[]"])
    {
        EducationInstitution * educationInstitution=[[EducationInstitution alloc]init];
        _list=[[NSArray alloc]initWithArray:[educationInstitution parseListEducationInstitutionFromJson:result]];
        resident.residentId=[[[NSUserDefaults standardUserDefaults]objectForKey:@"residentId"]integerValue];
        [self.connection connectionToService:@"GetEducationInstitutionsToResident" jsonDictionary:[resident parseResidentIdToDict] controller:self withSelector:@selector(GetEducationInstitutionsToResidentResult:)];
    }
    else
    {
        NSLog(@"Empty reports list");
        [self.generic hideNativeActivityIndicator:self];
    }
}

-(void)GetEducationInstitutionsToResidentResult:(NSString*)result
{
    [self.generic hideNativeActivityIndicator:self];
    if(![result isEqualToString:@""]&&![result isEqualToString:@"[]"])
    {
        EducationInstitution * educationInstitution=[[EducationInstitution alloc]init];
        choose=[[NSArray alloc]initWithArray:[educationInstitution parseListEducationInstitutionFromJson:result]];
    }
    else
        NSLog(@"Empty interestArea list");
     //indexx=0;
    [_tbl reloadData];
}
-(void)SetEducationInstitutionsToResidentResult:(NSString*)result
{
    [self.generic hideNativeActivityIndicator:self];
    if([result isEqualToString:@"true"])
    {
        [UIView animateWithDuration:0.5
                              delay:.2f
                            options: UIViewAnimationOptionAllowAnimatedContent
                         animations:^{ [self.view setAlpha:0];
                         }
                         completion:^(BOOL finished){
                             [self.view removeFromSuperview];
                         }];    
    }
    else
        NSLog(@"Empty reports list");
}
-(void)SettingsCellDelegateClick:(int) index ischeck:(BOOL) isCheck;
{
    NSLog([NSString stringWithFormat:@"%@ %d",@"count list",choose.count]);
  NSLog([NSString stringWithFormat:@"%@ %d",@"index",index]);
   //   NSLog([NSString stringWithFormat:@"%@ %d",@"global index",indexx]);
    NSArray* filt=[[NSArray alloc]init];
    
    if(isCheck){
     InterestArea * interestArea=[[InterestArea alloc]init];
    interestArea=((InterestArea*)_list[index]);
    [choose addObject:interestArea];
    }
    else
    {
        
             filt = [choose filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"interestAreaId =[cd] %d",((InterestArea*)_list[index]).interestAreaId]];
        if(filt.count>0)
        {
            InterestArea* f=((InterestArea*)filt[0]);
            for(int v=0;v<choose.count;v++)
            {   InterestArea* c=((InterestArea*)choose[v]);

                if(c.interestAreaId==f.interestAreaId)
                    [choose removeObjectAtIndex:v];
            }
        }
    }

}
@end
