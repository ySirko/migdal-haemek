//
//  EmergencySubViewController.h
//  NewCrmc
//
//  Created by MyMac on 8/7/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Connection.h"
#import "Generic.h"
#import "AppDelegate.h"

@interface EmergencySubViewController : UIViewController
@property(retain,nonatomic)AppDelegate*appDelegate;
@property(retain,nonatomic)Connection*connection;
@property(retain,nonatomic)Generic*generic;
@property(retain,nonatomic)UILabel*lbl1;
@property(retain,nonatomic)UILabel*lbl2;
@property(retain,nonatomic)UIImageView*img;
@property(retain,nonatomic)UILabel*lbl3;
@property(retain,nonatomic)UILabel*lbl4;
@property(retain,nonatomic)UILabel*lbl5;
@property(retain,nonatomic)UILabel*lbl6;
@property(retain,nonatomic)UIButton*btn;
@property(retain,nonatomic) UIImageView *imgIcon;
@property int index;
@end
