//
//  CouncilOfficialCell.h
//  CRMC
//
//  Created by Racheli Kroiz on 05/05/14.
//  Copyright (c) 2014 Racheli Kleinhendler. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "Entity.h"
@protocol CouncilOfficialCellDelegate <NSObject>
-(void)CouncilOfficialCellPresentMail:(MFMailComposeViewController*)mailCompose;
-(void)CouncilOfficialCellCall:(Entity*) entity;
-(void)CouncilOfficialCellInfo:(Entity*) entity;
@end

@interface CouncilOfficialCell : UITableViewCell<MFMailComposeViewControllerDelegate,CouncilOfficialCellDelegate>

@property (retain,nonatomic) id<CouncilOfficialCellDelegate> delegate;
@property(retain,nonatomic) UILabel* lblNameCouncilOfficial;
@property(retain,nonatomic) UIButton* imgPhone;
@property(retain,nonatomic) UIButton* imgEmail;
@property(retain,nonatomic) UIButton* imgInfo;
@property(retain,nonatomic)Entity* entity;
-(void)setCouncilOfficialCell:(Entity*)entity;
@property(retain,nonatomic)UIColor *color;
@property(retain,nonatomic)NSString*strColor;
@end