//
//  EventDetailsViewController.h
//  CRMC
//
//  Created by WebitMacMini Two on 6/18/14.
//  Copyright (c) 2014 Racheli Kleinhendler. All rights reserved.
//

#import "Event.h"
#import "SubViewController.h"
#import "UIHyperlinksButton.h"
@interface EventDetailsViewController :SubViewController
@property (strong, nonatomic)  UILabel *lblSubTitle;
@property (strong, nonatomic)  UILabel *lblDate;
@property (strong, nonatomic)  UILabel *lblAddress;
@property (strong, nonatomic)  UILabel *lblPrice;
@property (strong, nonatomic)  UILabel *lblNots;
@property (strong, nonatomic)  UITextView *lblDescription;
@property(retain,nonatomic)Event*event;
@property(retain,nonatomic)NSString* header;
@property (strong, nonatomic)  UILabel *lblStrDate;
@property (strong, nonatomic)  UILabel *lblStrAdderss;
@property (strong, nonatomic)  UILabel *lblStrDescription;
@property (strong, nonatomic)  UILabel *lblStrPrice;
@property (strong, nonatomic)  UILabel *lblStrNote;
@property int index;
@property (strong, nonatomic)  UIHyperlinksButton *btnLink;
@property(strong,nonatomic)UIColor*color;
@property (retain, nonatomic) UIWebView *webDescription;
@end
