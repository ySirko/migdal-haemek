//
//  EventCell.m
//  CRMC
//
//  Created by WebitMacMini Two on 6/15/14.
//  Copyright (c) 2014 Racheli Kleinhendler. All rights reserved.
//

#import "EventCell.h"

@implementation EventCell

-(void)setEventCell:(Event*)event
{
    [self setDynamicObject];
    _lblTitle.text=event.title;
    if ([event isKindOfClass:[Event class]]) {
        if(!(event.eventDate == [NSNull null]))
            _lblDate.text=[NSString stringWithFormat:@"תאריך: %@",event.eventDate];
        if(![event.address isEqualToString:@""] )
            _lblAddress.text=[NSString stringWithFormat:@"מקום: %@",event.address];
    }    
}
-(void)setEventRssCell:(Event*)event
{
    [self setDynamicObject];
    _lblTitle.text=event.title;
    _lblDate.text=event.eventDate;
}
- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    if(_isHighlighted)
    {
        if(highlighted)
        {
            _imgOpen.imageView.image=[UIImage imageNamed:_highlightedChetz];
            _line.backgroundColor=_highlightedColor;
        }
        else
        {
            _imgOpen.imageView.image=[UIImage imageNamed:_chetz];
            _line.backgroundColor=_color;
        }
    }
}
-(void)setDynamicObject
{
    //add top Label
    _lblTitle=[[UILabel alloc]init];
    _lblTitle.frame=CGRectMake(34 , 0, 275, 15);
    _lblTitle.font=[UIFont fontWithName:@"Helvetica-Bold" size:13];
    [_lblTitle setTextAlignment:NSTextAlignmentRight];
    [self addSubview:_lblTitle];

  if(_index!=13&&_index!=14)
  {
    _lblDate=[[UILabel alloc]initWithFrame:CGRectMake(34, 15, 275, 15)];
    _lblDate.font=[UIFont fontWithName:@"Helvetica" size:12];
    [_lblDate setTextAlignment:NSTextAlignmentRight];
    [self addSubview:_lblDate];
   
    _lblAddress=[[UILabel alloc]initWithFrame:CGRectMake(34, 28, 275, 15)];
    _lblAddress.font=[UIFont fontWithName:@"Helvetica" size:12];
    [_lblAddress setTextAlignment:NSTextAlignmentRight];
    [self addSubview:_lblAddress];
  }
    if (_index!=14)
    {
    //add button navigation
    _imgOpen=[[UIButton alloc]initWithFrame:CGRectMake(14, 11, 17, 26)];
    UIImage *btnImage = [UIImage imageNamed:_chetz];
    [_imgOpen setImage:btnImage forState:UIControlStateNormal];
    [self addSubview:_imgOpen];
    }
    ///add buttom label
    _line=[[UILabel alloc]initWithFrame:CGRectMake(0, 50, 320, 4)];
    _line.backgroundColor=_color;
    [self addSubview:_line];
  
}
-(void)setSemechDynamicObject
{
    //add top Label
    _lblTitle=[[UILabel alloc]init];
    _lblTitle.frame=CGRectMake(34 , 0, 275, 15);
    _lblTitle.font=[UIFont fontWithName:@"Helvetica-Bold" size:13];
    [_lblTitle setTextAlignment:NSTextAlignmentRight];
    [self addSubview:_lblTitle];
    
    
    ///add buttom label
    _line=[[UILabel alloc]initWithFrame:CGRectMake(0, 50, 320, 4)];
    _line.backgroundColor=_color;
    [self addSubview:_line];
    
}
-(void)setSemechCell:(Event*)event
{
    [self setSemechDynamicObject];
    _lblTitle.text=event.title;
    //_lblDate.text=[NSString stringWithFormat:@"תאריך: %@",event.eventDate];
    if(!(event.address ==[NSNull null]))
    _lblAddress.text=[NSString stringWithFormat:@"מקום: %@",event.address];
}

@end
