//
//  iCodeBlogAnnotation.m
//  CRMC
//
//  Created by Lior Ronen on 5/5/14.
//  Copyright (c) 2014 neomi&batya&shuli. All rights reserved.
//

#import "iCodeBlogAnnotation.h"

@implementation iCodeBlogAnnotation
-init
{
	return self;
}

-initWithCoordinate:(CLLocationCoordinate2D)inCoord
{
	_coordinate = inCoord;
	return self;
}

-initWith:(CLLocationCoordinate2D)inCoord Title:(NSString*)title AnnotationType:(int)annotationType
{
    _coordinate = inCoord;
    _title=title;
    _annotationType=annotationType;
    return self;
}
@end
