//
//  iCodeBlogAnnotation.h
//  CRMC
//
//  Created by Lior Ronen on 5/5/14.
//  Copyright (c) 2014 neomi&batya&shuli. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface iCodeBlogAnnotation : NSObject<MKAnnotation>
@property (nonatomic) CLLocationCoordinate2D coordinate;
@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSData *icon;
@property int annotationType;
-initWith:(CLLocationCoordinate2D)inCoord Title:(NSString*)title AnnotationType:(int)annotationType;
-initWithCoordinate:(CLLocationCoordinate2D)inCoord;
@end
