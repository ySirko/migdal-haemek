//
//  CategoryCell.m
//  CRMC
//
//  Created by Lior Ronen on 5/5/14.
//  Copyright (c) 2014 neomi&batya&shuli. All rights reserved.
//

#import "CategoryCell.h"

@implementation CategoryCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setCategoryCell
{
    _x_v=[[UILabel alloc]initWithFrame:CGRectMake(119, 5, 15, 15)];
     _imagechecked=[[UIImageView alloc]initWithFrame:CGRectMake(120, 6, 12, 12)];
    [self addSubview:_x_v];
    [self addSubview:_imagechecked];
    _x_v.layer.borderColor = [UIColor blackColor].CGColor;
    _x_v.layer.borderWidth = 1.0;
    _imagechecked.image=[UIImage imageNamed:@"vMap.png"];
}

@end
