//
//  iCodeMapViewController.m
//  CRMC
//
//  Created by Lior Ronen on 5/5/14.
//  Copyright (c) 2014 neomi&batya&shuli. All rights reserved.
//

#import "iCodeMapViewController.h"

@interface iCodeMapViewController ()
{
    AppDelegate *appDelegate;
    NSArray* filterArray ;
    CGFloat height;
    int animateWidth;
}
@end

@implementation iCodeMapViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName=@"iCodeMapViewController";
    _mapView=[[MKMapView alloc]initWithFrame:CGRectMake(0, 79, 320, self.view.frame.size.height-79)];
    _mapView.delegate=self;
    [self.view addSubview:_mapView];
    _btnInerestPoint=[[UIButton alloc]initWithFrame:CGRectMake(8, 91, 82, 34)];
     [_btnInerestPoint setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@Points.png",_strColor]] forState:normal];
    [_btnInerestPoint addTarget:self action:@selector(btnBarClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_btnInerestPoint];
    self.logo.hidden=YES;
    self.buildings.hidden=YES;
    self.lblBackgroundTitle.frame=CGRectMake(0, 50, 320, 29);
    self.Title.frame=CGRectMake(0, 44, 304, 34);
    self.lblBackgroundTitle.backgroundColor=_color;
    self.Title.text=self.header;
    _listCategory=[[NSArray alloc]init];
    [[_mapView layer]setCornerRadius:4];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
     _listPoints=[[NSMutableArray alloc]init];
    height =25;
    _categoryTableView = [[UITableView alloc]init];
    _categoryTableView.rowHeight = height;
    _categoryTableView.layer.borderColor=_color.CGColor;
    _categoryTableView.layer.borderWidth=10;
    [[_categoryTableView layer]setBorderWidth:0.7];
    [[_categoryTableView layer]setCornerRadius:4];
    _categoryTableView.delegate=self;
    _categoryTableView.dataSource=self;
    [self.view addSubview:_categoryTableView];
    [self.generic showNativeActivityIndicator:self];
    if(self.appDelegate.city.cityId!=1)
        [self.connection connectionToService:@"GetInterestPointToCity" jsonDictionary:[appDelegate.city parseCityToDict] controller:self withSelector:@selector(GetInterestPointToCityResult:)];
}

-(void)GetInterestPointToCityResult:(NSString*)result
{
    [self.generic hideNativeActivityIndicator:self];
    if(![result isEqualToString:@""]&&![result isEqualToString:@"[]"])
    {
        Categories * categories=[[Categories alloc]init];
        _listCategory=[[NSArray alloc]initWithArray:[categories parseListCategoriesFromJson:result]];
        _categoryTableView.frame = CGRectMake((self.view.frame.size.width-140)/2,230, 150, 0);
        for(Categories*cat in _listCategory)
            for(InterestPoint*ip in cat.interestPoint)
            {
                CLLocationCoordinate2D workingCoordinate;
                workingCoordinate.latitude = ip.lat.doubleValue;
                workingCoordinate.longitude = ip.lon.doubleValue;
                iCodeBlogAnnotation *annotation = [[iCodeBlogAnnotation alloc] init];
                annotation.coordinate=workingCoordinate;
                annotation.icon=ip.icon;
                [annotation setTitle:ip.descriptionn];
                [annotation setAnnotationType:ip.categoryId];
                [_listPoints addObject:annotation];
            }
        [_mapView addAnnotations:_listPoints];
        [self focusMapOnAnnotation];
        [_categoryTableView reloadData];
    }
    else
        NSLog(@"Empty categoties list");
}

-(void)focusMapOnAnnotation
{
    MKMapRect zoomRect = MKMapRectNull;
    for (id <MKAnnotation> annotation in _mapView.annotations)
    {
        MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
        MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0.1, 0.1);
        zoomRect = MKMapRectUnion(zoomRect, pointRect);
    }
    [_mapView setVisibleMapRect:zoomRect edgePadding:UIEdgeInsetsMake(+80, +30, +30, +30) animated:YES];
}

- (IBAction)btnBarClick:(id)sender
{
    if(_categoryTableView.frame.size.height==0)
        animateWidth=_listCategory.count*height;
    else
        animateWidth=0;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [UIView setAnimationDelay:.5f];
    [UIView setAnimationDuration:0.5];
     _categoryTableView.frame = CGRectMake((self.view.frame.size.width-140)/2, 230
, 150, animateWidth);
    [UIView commitAnimations];
}

- (iCodeBlogAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
	iCodeBlogAnnotationView *annotationView = nil;
	iCodeBlogAnnotation* myAnnotation = (iCodeBlogAnnotation *)annotation;
    iCodeBlogAnnotationView *newAnnotationView = (iCodeBlogAnnotationView *)[self.mapView dequeueReusableAnnotationViewWithIdentifier:myAnnotation.title];
    if(nil == newAnnotationView)
        newAnnotationView = [[[iCodeBlogAnnotationView alloc] initWithAnnotation:myAnnotation reuseIdentifier:myAnnotation.title] autorelease];
    annotationView = newAnnotationView;
	[annotationView setEnabled:YES];
	[annotationView setCanShowCallout:YES];
    return annotationView;
}

#pragma CategoryTable
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   return _listCategory.count;

}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{  
    static NSString *CellIdentifier = @"Cell";
    CategoryCell *cell = (CategoryCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell = [[CategoryCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    cell.categoryName=[[UILabel alloc]initWithFrame:CGRectMake(20, 2, 80, 21)];
    cell.categoryName.textAlignment=NSTextAlignmentRight;
    cell.categoryName.font=[UIFont fontWithName:@"Helvetica Neue" size:14.0];
    [cell addSubview:cell.categoryName];
     cell.categoryName.text=((Categories*)_listCategory[indexPath.row]).categoryName;
    [cell setCategoryCell];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CategoryCell* currentCell=_categoryTableView.visibleCells[indexPath.row];
    filterArray = [_listPoints filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"annotationType =[cd] %d",((Categories*)_listCategory[indexPath.row]).CategoryId]];
    if([currentCell.imagechecked.image isEqual:[UIImage imageNamed:@"vMap.png"]])
    {
        [_mapView removeAnnotations:filterArray];
        currentCell.imagechecked.image=[UIImage imageNamed:@"xMap.png"];
    }
    else
    {
        [_mapView addAnnotations:filterArray];
         currentCell.imagechecked.image=[UIImage imageNamed:@"vMap.png"];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    animateWidth=0;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [UIView setAnimationDelay:.5f];
    [UIView setAnimationDuration:0.5];
    _categoryTableView.frame = CGRectMake((self.view.frame.size.width-140)/2, 230
                                          , 150, animateWidth);
    [UIView commitAnimations];
}
@end
