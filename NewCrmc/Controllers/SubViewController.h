//
//  SubViewController.h
//  NewCrmc
//
//  Created by WebitMacMini Two on 7/17/14.
//  Copyright (c) 2014 webit. All rights reserved.
//
#import "Connection.h"
#import "Generic.h"
#import "AppDelegate.h"
#import "AppManager.h"
#import "GAITrackedViewController.h"
@interface SubViewController : GAITrackedViewController
@property(retain,nonatomic)AppDelegate*appDelegate;
@property(retain,nonatomic)Connection*connection;
@property(retain,nonatomic)Generic*generic;
@property(retain,nonatomic)UILabel*lblBackgroundTitle;
@property(retain,nonatomic)UILabel*Title;
@property(retain,nonatomic)UIImageView* buildings;
@property(retain,nonatomic)UIImageView*logo;
//@property(retain,nonatomic)UIColor*lightBlue;
//@property(retain,nonatomic)UIColor*pink;
//@property(retain,nonatomic)UIColor*purple;
//@property(retain,nonatomic)UIColor*green;
//@property(retain,nonatomic)UIColor*orange;
//@property(retain,nonatomic)UIColor*lightBlueHighlighted;
//@property(retain,nonatomic)UIColor*pinkHighlighted;
//@property(retain,nonatomic)UIColor*purpleHighlighted;
//@property(retain,nonatomic)UIColor*greenHighlighted;
//@property(retain,nonatomic)UIColor*orangeHighlighted;
//@property(retain,nonatomic)UIColor*redHighlighted;
//@property(retain,nonatomic)NSString*deviceName;;
//@property(retain,nonatomic)UIColor*lightBlue2;
//@property(retain,nonatomic)UIColor*pink2;
//@property(retain,nonatomic)UIColor*purple2;
//@property(retain,nonatomic)UIColor*green2;
//@property(retain,nonatomic)UIColor*orange2;
//@property(retain,nonatomic)UIColor*red2;
//@property(retain,nonatomic)UIColor*lightBlueHighlighted2;
//@property(retain,nonatomic)UIColor*pinkHighlighted2;
//@property(retain,nonatomic)UIColor*purpleHighlighted2;
//@property(retain,nonatomic)UIColor*greenHighlighted2;
//@property(retain,nonatomic)UIColor*orangeHighlighted2;
//@property(retain,nonatomic)UIColor*redHighlighted2;
@property(retain,nonatomic)AppManager*appManager;
@property(retain,nonatomic)UIImageView*background;
@property(retain,nonatomic)NSString*header;
@property(retain,nonatomic)UIButton*bt;
-(NSString *)yesButWhichDeviceIsIt;
@end
