//
//  MapImageViewController.m
//  מכון ויצמן
//
//  Created by Lior Ronen on 2/2/15.
//  Copyright (c) 2015 webit. All rights reserved.
//

#import "MapImageViewController.h"

@interface MapImageViewController ()

@end

@implementation MapImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.logo.hidden=YES;
    self.buildings.hidden=YES;
    self.lblBackgroundTitle.frame=CGRectMake(0, 50, 320, 29);
    self.Title.frame=CGRectMake(0, 44, 304, 34);
    self.lblBackgroundTitle.backgroundColor=_color;
    self.Title.text=self.header;

//    self.logo.hidden=YES;
//    self.buildings.hidden=YES;
//    self.lblBackgroundTitle.frame=CGRectMake(0, 50, 320, 29);
//    self.Title.frame=CGRectMake(0, 44, 304, 34);
//    self.lblBackgroundTitle.backgroundColor=_color;
//    self.Title.text=self.header;
        _mapImg=[[UIImageView alloc]initWithFrame:CGRectMake(0, 79, 320, self.view.frame.size.height-79)];
    _mapImg.image=[UIImage imageNamed:@"HaderaMap.jpg"];
    _mapImg.contentMode = UIViewContentModeScaleAspectFill;
    _mapImg.clipsToBounds = YES;
    [self.view addSubview:_mapImg];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
