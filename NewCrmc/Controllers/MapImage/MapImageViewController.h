//
//  MapImageViewController.h
//  מכון ויצמן
//
//  Created by Lior Ronen on 2/2/15.
//  Copyright (c) 2015 webit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SubViewController.h"
@interface MapImageViewController : SubViewController
@property (retain, nonatomic)UIColor*color;
@property (retain, nonatomic)UIImageView*mapImg;
@end
