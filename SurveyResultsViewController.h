//
//  SurveyResultsViewController.h
//  NewCrmc
//
//  Created by MyMac on 8/6/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

@interface SurveyResultsViewController : SubViewController
@property (retain, nonatomic)  UILabel *lblSurveyDescription;
@property (retain, nonatomic)  UILabel *lbl2;
@property (retain, nonatomic) UITableView *tblResult;
@property (retain, nonatomic) UILabel*line1;
@property (retain, nonatomic) UILabel*line2;
@property(retain,nonatomic)UIColor*color;
@end
